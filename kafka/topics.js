const topics = {
  settingsService: {
    request: 'settings_request',
    response: 'settings_response'
  },
  UserActivityService: {
    request: 'user_active_request',
    response: 'user_active_response'
  },
  NotifyService: {
    request: 'notify_request',
    response: 'notify_response'
  }
};

module.exports = topics;
