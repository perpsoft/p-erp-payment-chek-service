const { Kafka } = require('kafkajs');
const EventEmitter = require('events');
const config = require('../config');
const log = require('../config/logger');
const topics = require('./topics');
const reducer = require('./reducer');

const { responseCreator, makeMessage } = require('./functions');

const validateMessage = require('./validation');
const {
  SETTINGS_INIT,
  RESPONSE_RECIEVED,
  REQUEST_TIMEOUT
} = require('../constants');

const kafka = new Kafka({...config.get('kafka'), brokers: process.env.KAFKA.split(',')});

let pendings = [];

class KafkaInit {
  constructor(groupId) {
    this.producer = kafka.producer();
    this.consumer = kafka.consumer({groupId: groupId});
    this.requestTopic = config.get('kafka:requestTopic');
    this.responseTopic = config.get('kafka:responseTopic');
  }

  async send(topic, message) {
    return new Promise( async (resolve, reject) => {
      const pending = {
        date: new Date(),
        requestKey: message.requestKey,
        requestEmitter: new EventEmitter()
      };

      pendings.push(pending);

      await this.producer.send({
        topic: topic,
        messages: [{
          value: JSON.stringify(message)
        }],
      });

      pending.requestEmitter.on(RESPONSE_RECIEVED, (res) => {
        if (res.status && res.status.code === 200) {
          resolve(res);
        } else {
          reject(res);
        }
        this.responseHandler(res);
      });

      pending.requestEmitter.on(REQUEST_TIMEOUT, () => {
        resolve({
          status: {
            code: 408,
            message: 'Request Timeout'
          }
        });
      });

    });
  }


  sendResponse(body) {
    return this.producer.send({
      topic: this.responseTopic,
      messages: [{
        value: JSON.stringify(body)
      }],
    });
  }


  responseHandler (response) {
    let index = pendings.findIndex( (pending) => pending.requestKey === response.action.requestKey);
    if (index) {
      pendings.splice(index, 1);
    }
  }


  pendingHandler () {
    const interval = config.get('kafka:pendingInterval');
    let updatedPending;
    setInterval(() => {
      const now = new Date().getTime();

      updatedPending = pendings.map( (pending) => {
        if ((pending.date.getTime() + interval) > now) {
          return pending;
        } else {

          pending.requestEmitter.emit(REQUEST_TIMEOUT);
          return null;
        }
      }).filter( item => item);
      pendings = updatedPending;
    }, interval);
  }

  async init() {
    await this.producer.connect();
    await this.consumer.connect();

    await this.consumer.subscribe({topic: this.requestTopic, fromBeginning: true});
    await this.consumer.subscribe({topic: this.responseTopic});

    Object.keys(topics).forEach(async service => await this.consumer.subscribe({
      topic: service.response
    }));

    await this.consumer.run({
      eachMessage: async ({topic, partition, message}) => {

        const request = JSON.isJsonString(message.value.toString()) && JSON.parse(message.value.toString());
        console.log('MESSAGE_JSON', request);

        if (topic === this.requestTopic) {

          if (!request) {
            return log.error({action: 'MESSAGE_RECIVED', code: 460});
          }

          const valid = validateMessage(request);

         // console.log('MESSAGE_VALID', valid)

          if (valid !== true) {
            if (request.headers && request.headers.service) {
              this.sendResponse(responseCreator(400, request, valid));
            }
            return log.error({action: 'MESSAGE_NOT_VALID', message: valid});
          }

          reducer(topic, partition, request)
            .then(data => {
              console.log('data', data);
              if (data) {
                this.sendResponse(responseCreator(200, request, data));
              } else {
                this.sendResponse(responseCreator(400, request, 'Wrong action name'));
              }
            })
            .catch(err => {
              console.log('errKafka:', err.message);
              this.sendResponse(responseCreator(400, request, err.message) ) ;
            });

        } else {

          if (request.action) {
            let pending = pendings.find( (pen) => pen.requestKey === request.action.requestKey);
            if (pending) {
              pending.requestEmitter.emit('RESPONSE_RECIEVED', request);
            }
          }
        }
      },
    });

    this.pendingHandler();

    await this.send(topics.settingsService.request, makeMessage(
      SETTINGS_INIT,
      {
        settings: {
          kafka: config.get('kafka'),
          payment: config.get('payment'),
          port: config.get('port')
        }
      }
    ));

  }

}

const kafkaInit = new KafkaInit(config.get('kafka:clientId'));

module.exports = kafkaInit;
