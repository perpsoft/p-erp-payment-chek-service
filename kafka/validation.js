const Joi = require('@hapi/joi');


// {"data": {"testProp": "ff"}, "action": {"type": "action", "message": "SETTINGS_UPDATE", "requestKey": "1"}, "headers": { "service": {"name": "name", "version": "12", "ip": "172.17.0.1"} } }

const schema = Joi.object({
  action: Joi.object({
    type: Joi.string().only(['action', 'signal']).required(),
    message: Joi.string().required(),
    requestKey: Joi.string().required()
  }).required(),
  headers: Joi.object({
    service: Joi.object({
      name: Joi.string().required(),
      version: Joi.string().required(),
      ip: Joi.string().ip({version: ['ipv4']}).required()
    }).required()
  }).required(),
  data: Joi.object().required()
});


/*const errorHandler = (error) => {
  console.log('error', error)
 
  switch (error.type) {
    case 'any.required':
      return 449; 
    case 'string.base':
      return 111;
    default:
      return false;
  } 

}*/

const validateMessage = (message) => { 
  return schema.validate(message, /*{abortEarly: false},*/ (err, value) => { 
    if (err) {
      return err.details[0].message;
      //return errorHandler(err.details[0]);
    }
    return true;
  });
}

module.exports = validateMessage;