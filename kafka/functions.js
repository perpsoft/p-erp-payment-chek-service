const ip = require('ip');
const config = require('../config');

const statusCode = {
  200: 'OK',
  400: 'Bad request',
  401: 'Invalid JSON',
  449: 'Required parameter is missing'
};

function generateKey (length = 16) {
  return [...Array(length)].map(i=>(~~(Math.random()*36)).toString(36)).join('');
}

function responseCreator (code, req, data) {
  return {
    status: {
      code: code,
      message: statusCode[code] || 'Unknown error code'
    },
    headers: {
      service: {
        name: config.get('X-Service-Name'),
        version: config.get('X-Service-Version'),
        ip: ip.address()
      },
      target: req.headers.service
    },
    ...data && { data: data },
    action: req.action
  };
}

function makeMessage (action, data) {
  return {
    action: {
      type: 'action',
      message: action,
      requestKey: generateKey()
    },
    headers: {
      service: {
        name: config.get('X-Service-Name'),
        version: config.get('X-Service-Version'),
        ip: ip.address()
      }
    },
    data: data
  }
}

function mergeDeep (...objects) {
  const isObject = obj => obj && typeof obj === 'object';

  return objects.reduce((prev, obj) => {
    Object.keys(obj).forEach(key => {
      const pVal = prev[key];
      const oVal = obj[key];

      if (Array.isArray(pVal) && Array.isArray(oVal)) {
        prev[key] = pVal.concat(...oVal);
      }
      else if (isObject(pVal) && isObject(oVal)) {
        prev[key] = mergeDeep(pVal, oVal);
      }
      else {
        prev[key] = oVal;
      }
    });

    return prev;
  }, {});
}

module.exports.responseCreator = responseCreator;
module.exports.makeMessage = makeMessage;
module.exports.mergeDeep = mergeDeep;
