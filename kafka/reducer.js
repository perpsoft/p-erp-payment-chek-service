const mergeSettings = require('./settings');

const {
  SETTINGS_UPDATE,
} = require('../constants');


const reducer = async function(topic, partition, request){

  switch (request.action.message) {

   // {"data": {"testProp": "ff"}, "action": {"type": "action", "message": "SETTINGS_UPDATE", "requestKey": "1"}, "headers": { "service": {"name": "name", "version": "12", "ip": "172.17.0.1"} } }
    case SETTINGS_UPDATE:

      return mergeSettings(request.data);

    /*
    ...

    other cases

    ...
    */

    default:
      return false;
  }
  
} 



module.exports = reducer;
