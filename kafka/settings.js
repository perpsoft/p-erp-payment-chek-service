const fs = require('fs');
const { mergeDeep } = require('./functions');
const filePathes = {
  json: './config/config.json',
  tmp: './config/config.tmp',
  backup: './config/config.backup'
}

const settings = function (message) {

  return new Promise( (resolve, reject) => {

    fs.readFile(filePathes.json, 'utf8', function (err, data) {
      if (err) reject({message: err.message});
     
      const obj = JSON.parse(data);
      const mergedConfig = mergeDeep(obj, message.settings);
      const jsonStr = JSON.stringify(mergedConfig, null, '\t');

      fs.writeFile(filePathes.tmp, jsonStr, 'utf8', function(err){
        if(err) reject({message: err.message});
        
        fs.rename(filePathes.json, filePathes.backup, (err) => {
          if (err) reject({message: err.message});

          fs.rename(filePathes.tmp, filePathes.json, (err) => {
            if (err) reject({message: err.message});

            throw 'restart app';
          });
        });
      });
    });
  });


};

module.exports = settings;