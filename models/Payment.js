/**
 * @namespace
 * @name Models
 * @description Класи моделей
 */

const mongoose = require('../config/mongoose');
const Schema = mongoose.Schema;



/**
 * @class Payment
 * @name Models.Payment
 * @description модель Платежів
 * @memberOf Models
 */

const schema = new Schema({
  Doc_id: Number,
  Doc_type: Number,
  Doc_num: String,
  Date_reg: String,
  Date_getpay: String,
  Doc_date: String,
  Currency_code: Number,
  Doc_sum: Number,
  Doc_sum_base: Number,
  Doc_cource: Number,
  payerName: String,
  obj_num: String,
  pay_code: String
});


/* MSSQL types:

•	Doc_id int  - Індекс платіжного документа
•	Doc_type int  -Тип платіжного документа
•	Doc_num varchar(32) – Номер платіжного документа
•	Date_reg datetime  - Дата реєстрації платіжного документа в Укрпатенті
•	Date_getpay datetime – Дата зарахування коштів
•	Doc_date datetime – Дата платежу
•	Currency_code integer  -Код валюти
•	Doc_sum money – Фактично сплачена сума
•	Doc_sum_base money – Сума, яку необхідно сплатити
•	Doc_cource money – Курс НБУ на дату платежу (відповідно до валюти)
•	payerName varchar(100) - Платник
•	obj_num varchar(100) – Номер ОПІВ
•	pay_code varchar(20) – Код типу платежу
*/

/**
 * @alias Payment.prototype.getByIds
 * @name Models.Payment.getByIds
 *
 * @method
 * @description Отримання платежів по масиву ids
 * @param {String|Array<String>} ids - id платежів
 *
 * @returns {Promise<Array>} - проміс, який містить масив платежів
 */
schema.statics.getByIds = async function (ids) {
  const arr = Array.isArray(ids) ? ids : [ids];
  return await this.find({}).where("id").in(arr);
}


/**
 * @alias Payment.prototype.getById
 * @name Models.Payment.getById
 *
 * @async
 * @method
 * @description Отримання платежу по id
 * @param {String} id - id платежу
 *
 * @returns {Promise<Object>} - проміс, який містить об'єкт платежа
 */
schema.statics.getById = async function (id) {
  return await this.findOne({id: id});
};


/**
 * @alias Payment.prototype.removeAll
 * @name Models.Payment.removeAll
 *
 * @async
 * @method
 * @description Метод видяляе всі платежі з БД 
 *
 * @returns {Promise<Object>} - проміс, який містить інформацію про кількість видаленніх платежів
 */
schema.statics.removeAll = async function () {
  return await this.deleteMany({});
};

/**
 * @alias Payment.prototype.add
 * @name Models.Payment.add
 *
 * @async
 * @method
 * @description Метод додає користувачів в БД
 * @param {Object|Array<Object>} payment - Об'єкт платежа АБО масив платежів
 * @param {Object} payment - об'єкт платежа згідно схеми моделі
 * @param {String} payment.id - ID платежа
 * @param {String} payment.date
 * 
 * @example [{
    },
    ...
    ]
 *   
 * @returns {Promise<Array>} - проміс, який містить массив об'єктів доданих платежів згідно схеми моделі
 *
 */

schema.statics.add = async function (docs) {
  const list = Array.isArray(docs) ? docs : [docs];
  return await this.insertMany(list);
}

/**
 * @alias Payment.prototype.check
 * @name Models.Payment.check
 *
 * @method
 * @description Отримання платежів для перевірки
 *
 * @returns {Promise<Array>} - проміс, який містить масив платежів
 */

schema.statics.check = async function () {
  const exp = `this.date < Date.now()`;
  return await this.find({$where: exp});
}



/**
 * @alias Payment.prototype.updateById
 * @name Models.Payment.updateById
 *
 * @async
 * @method
 * @description Метод знаходиьть платіж по ID та змінюе інформацію в БД
 * @param {String} id - ID платежа в системі
 * @param {Object} payment - об'єкт платежа згідно схеми моделі
 * @param {String} payment.id - ID платежа
 * @param {String} payment.date - 

 * 
 * @example { "id": "2",
      "date": 1567075924,
    }
 *   
 * @returns {Promise<Object>} - проміс, який містить об'єкт зміненого платежа
 *
 */
schema.statics.updateById = async function (id, data) {
  const options = {
    upsert: true,
    runValidators: true,
    new: true
  };
  return await this.findOneAndUpdate({id: id}, data, options);
};


/**
 * @alias Payment.prototype.removeById
 * @name Models.Payment.removeById
 *
 * @async
 * @method
 * @description Метод видяляе платіж з БД по ID
 * @param {string} id -  ID платежа в системі
 *
 * @returns {Promise<Object>} - проміс, який містить інформацію про кількість видаленних платежів
 */
schema.statics.removeById = async function (id) {
  return await this.deleteOne({id: id});
};

const Payment = mongoose.model('Payment', schema);

module.exports = Payment;
