const Kafka = require('../kafka');
const Postgres = require('../modules/Postgres');
const {makeMessage} = require('../kafka/functions');
const topics = require('../kafka/topics');

const sendAlert = (code, CEADpayment) => {
  const claimNumber = CEADpayment.obj_num;
  const userId = Postgres.getUserByClaimNumber(claimNumber);
  const codes = {
    3: "nachalnik_sector_data_podannya_tm",
    4: "nachalnik_sector_data_podannya_pz",
    5: "nachalnik_sector_data_podannya_kzpt"
  };
  const messTemplate = `Отримано новий платіж. Заявка ${claimNumber}`;

  const message = makeMessage("MESSAGE_ADD", {
    "status": "info",
    "sendingServer": ['browser'],
    "users": userId ? [userId] : [],
    "roles": [codes[code]],
    "text": messTemplate,
    "dispatchTime": "",
    "withToast": true
  });

  Kafka.send(topics.NotifyService.request, message);
};
