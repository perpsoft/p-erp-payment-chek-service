const Payment = require('../models/Payment');
const GetUnprocessedPaymentsTMPP = require('../config/mssql');
const cron = require('node-cron');
const config = require('../config');
const log = require('../config/logger');



/** Class Клас для імпорту платежів з ЦЕАД */
class PaymentImport {
  constructor(){
    this.isImporting = false;
  }

  /**
   * метод запускае розклад импорту платежів з ЦЕАД
   */
  init() {
    console.log('init');
    const interval = config.get('payment:import:interval');
    const valid = cron.validate(interval);

    if (!valid){
      const msg = `Invalid interval config: ${interval} for payment.import.interval`;
      throw new Error(msg);
    }
    cron.schedule(interval, () => {
      console.log('schedule: import.payment');
      this.importData()
        .catch(e => {
          log.error({action: 'IMPORT PAYMENT', error: e.message});
          console.log('import Payment data error', e)
        })
        .then(() => this.toggleStatus());
    });
  }

  /**
   * метод для импорту платежів з ЦЕАД
   */
  async importData() {
    if (this.isImporting) return;
    this.toggleStatus();

    log.info({action: 'IMPORT PAYMENT', msg: 'START'});
    console.log('import Payment data start');

    try {
      // const { recordset } = await GetUnprocessedPaymentsTMPP( 3||4||5 , (new Date).toISOString());

      // dummy data
      const data =
      {
        Doc_id: 1,
        Doc_type: 1,
        Doc_num: 1,
        Date_reg: Date.now(),
        Date_getpay: Date.now(),
        Doc_date: Date.now(),
        Currency_code: 1,
        Doc_sum: 1000,
        Doc_sum_base: 1000,
        Doc_cource: 1,
        payerName: 'String',
        obj_num: 'String',
        pay_code: 'String'
      };

      await Payment.add(data);
    } catch (error) {
      log.error({action: 'IMPORT PAYMENT', error: error.message});
      console.log('IMPORT PAYMENT', error.message)
    } finally {
      console.log('import Payment data end');
      log.info({action: 'IMPORT PAYMENT', msg: 'END'});
      this.toggleStatus();
    }
  }

  /**
   * змінюе статус процесу імпорту на протилежний
   */
  toggleStatus(){
    this.isImporting = !this.isImporting;
  }
}

module.exports = new PaymentImport();
