console.log('test4');
const {Pool} = require('pg');
const Joi = require('@hapi/joi');
const currencyCatalog = require('./currencyCatalog');

/** Class для запису платежів в БД */
class Postgres {

  /**
   * Create a point.
   * @param {number} dbOptions - Налаштування підключення до БД.
   */
  constructor(dbOptions) {
    this.roleClient = new Pool(Object.assign(dbOptions, {database: process.env.PG_DATABASE_USER}));
    this.claimClient = new Pool(Object.assign(dbOptions, {database: process.env.PG_DATABASE_CLAIM}));
  }

  /**
   * Метод повертає UTC UNIX timestamp з єкземпляру new Data()
   * @param {string} date - єкземпляр new Data()
   * @return {number} UTC UNIX timestamp
   */
  static _getUTCTimestamp(date) {
    const UTCDate = new Date(
      date.getUTCFullYear(),
      date.getUTCMonth(),
      date.getUTCDate(),
      date.getUTCHours(),
      date.getUTCMinutes(),
      date.getUTCSeconds()
    );
    return Math.floor(UTCDate/1000);
  }

  /**
   * Метод повертає користувача, що обробляє заявку на данному етапі
   * @param {string} number - номер заявки
   * @return {Object} Об'єкт користувача
   */
  async getUserByClaimNumber(number) {
    const caseCode = 'CS03009';
    const claimId = this._getClaimIdByNumber(number);
    const query = {
      text: `SELECT responsible_user_id FROM public.fv_claim_case
        WHERE case_code = ${caseCode}
        AND claimId = $1
        AND completed = 0
        `,
      values: [claimId],
    };
    try {
      const res = await this.claimClient.query(query);
      return res.rows[0];
    } catch (e) {
      console.log(e);
      return e;
    }
  }

  /**
   * Метод валідації платежа
   * @param {Object} object - Об'єкт данних платежа
   * @param {Object} schema - Схема об'єкта для валідації
   * @return {Object} Об'єкт ключів - значень або помилку.
   */
  static _checkPrepare(object, schema) {
    const result = Joi.validate(object, schema);
    if (result.error) {
      throw new Error(result.error);
    } else {
      return {
        keys: Object.keys(object),
        values: Object.values(object),
        str: Object.values(object).reduce((acc, c, i) => acc += (i ? ', ' : '') + `$${++i}`, '')
      }
    }
  }

  /**
   * Метод запису об'єкту платежа в БД
   * @param {Object} payment - Об'єкт данних платежа
   * @return {Object} Об'єкт записаного платежа
   */
  async setBankPayment(payment) {
    const pay = await this.fromCEADfactory(payment);

    const schema = {
      purpose: Joi.string().required(),
      payer: Joi.string().required(),
      amount: Joi.number().required(),
      currency: Joi.string().required(),
      operation_date: Joi.string().required(),
      register_date: Joi.string().required(),
      created_at: Joi.number().required(),
      updated_at: Joi.number().required(),
      base_amount: Joi.number().required(),
      rate: Joi.number().allow(null).required(),
      rate_date: Joi.string().required(),
      claim_id: Joi.string().required(),
      claim_number: Joi.string().required(),
      data_cead: Joi.string().required(),
      gov_tax_code: Joi.string().required(),
      counted_amount: Joi.number().required(),
      source_payment: Joi.string(),
      number: Joi.string()
    };
    try {
      const prepare = Postgres._checkPrepare(pay, schema);
      const query = {
        text: `INSERT INTO fv_bank_payment(${prepare.keys})
             VALUES (${prepare.str}) RETURNING *;`,
        values: prepare.values,
      };
      const payment = await this.claimClient.query(query);
      return payment.rows[0];
    } catch (err) {
      console.log(err);
      return err;
    }
  }

  /**
   * Метод для конвертації цифрового коду валюти в літеральний
   * @param {number} num - Цифровий код валюти
   * @return {string} Літеральний
   */
  static numToStrCurrency(num) {
    if (currencyCatalog[num]) return currencyCatalog[num];
    throw new Error('undefined currency')
  }

  async _getClaimIdByNumber(claimNumber) {
    const query = `SELECT id FROM public.fv_claim WHERE claim_number = $1`;
    try {
      const res = await this.claimClient.query(query, [claimNumber]);
      return res.rows[0].id;
    } catch (e) {
      console.log(e);
    }
  }

  /**
   * Метод-фабрика для конвертації об'єкта формата з ЦЕАД в об'єкт для запису в БД
   * @param {object} ceadTypeObj - Об'єкт формата ЦЕАД
   * @return {object} Об'єкт для запису в БД
   */
  async fromCEADfactory(ceadTypeObj) {
    const round = num => Math.floor(num * 1000000) / 1000000;
    const {
      pay_description,
      payerName,
      Doc_sum,
      Doc_cource,
      Currency_code,
      Date_reg,
      Date_getpay,
      Doc_date,
      obj_num,
      Pay_code,
      Doc_num
    } = ceadTypeObj;

    const amount = Doc_cource ? round(Doc_cource * Doc_sum) : Doc_sum;
    const UTCTimestamp = Postgres._getUTCTimestamp(new Date());

    return {
      purpose: pay_description,
      payer: payerName,
      amount: amount,
      currency: currencyCatalog[Currency_code],
      operation_date: Date_getpay,
      register_date: Date_reg,
      counted_amount: Doc_sum,
      rate: Doc_cource,
      rate_date: Doc_date,
      base_amount: Doc_sum,
      claim_id: await this._getClaimIdByNumber(obj_num),
      claim_number: obj_num,
      data_cead: JSON.stringify(ceadTypeObj),
      gov_tax_code: Pay_code,
      created_at: UTCTimestamp,
      updated_at: UTCTimestamp,
      source_payment: payerName,
      number: Doc_num
    }
  }
}

module.exports = new Postgres({
  user: process.env.PGUSER,
  host: process.env.PGHOST,
  password: process.env.PGPASSWORD,
  port: process.env.PGPORT,
});


