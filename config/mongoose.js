const config = require('../config');
const mongoose = require('mongoose');
const log = require('./logger');
mongoose.set('useFindAndModify', false);

const uri = config.get('mongoose:uri');
const options = config.get('mongoose:options');

mongoose.connectionPending = mongoose.createConnection(uri, options)
  .then((connection) => {
    return connection.dropDatabase();
  })
  .then( () => {
    return mongoose.connect(uri, options)
  })
  .then( () => log.info({'action': 'MONGOOSE CONNECT'}))
  .catch( err => {
    log.error({'action': 'MONGOOSE CONNECT'});
    console.log(err)
  } );

module.exports = mongoose;