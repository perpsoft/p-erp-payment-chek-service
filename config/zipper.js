const fs = require('fs');
const { zip } = require('zip-a-folder');

class ZipAFolder {

  static async main() {
    const docZipFile = './docs.zip';

    if (fs.existsSync(docZipFile)) {
      fs.unlinkSync(docZipFile,function(err){
        if(err) return console.log(err);
      });
    }

    await zip('./docs', docZipFile);
    console.log('DOCS READY: ' + docZipFile);
  }
}

ZipAFolder.main();
