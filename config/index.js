const nconf = require('nconf');
const path = require('path');

nconf.argv()
  .env()
  .file({ file: path.join(__dirname, 'config.json') });

nconf.set('mongoose:uri', process.env.MONGO || nconf.get('mongoose:test-uri'));

module.exports = nconf;
