const mongoose = require('mongoose');
const PaymentImport = require('../modules/PaymentImport');
const Kafka = require('../kafka');
const log = require('./logger');
const config = require('./index');
const shell = require('shelljs');
const Postgres = require('../modules/Postgres');

Date.prototype.addHours = function (h) {
  this.setHours(this.getHours() + h);
  return this;
};

Array.prototype.isEmpty = function () {
  return !this.length;
};

Array.prototype.onlyUnique = function () {
  return [...new Set(this)];
};

JSON.isJsonString = function (str) {
  try {
    JSON.parse(str);
  } catch (e) {
    return false;
  }
  return true;
};

const row = {
  "Doc_Id": 200000508328732,
  "doc_type": 1,
  "Doc_num": "165453",
  "Date_reg": "2020-01-15T00:00:00.000Z",
  "Date_getpay": "2020-01-15T00:00:00.000Z",
  "Doc_date": "2020-01-15T00:00:00.000Z",
  "Currency_code": "840",
  "Doc_sum": 501.52,
  "Doc_cource": 24.025702,
  "payerCode": "00032112",
  "payerName": "ВАТ УКРЕКСІМБАНК",
  "pay_id": 200000508322105,
  "category_code": "32",
  "obj_num": "m201815279",
  "Pay_code": "42700",
  "Pay_sum": 501.52,
  "IsUre": 0,
  "pay_description": "ЗБІР 42700 ЗА ПРОДОВЖЕННЯ ТЕРМІНУ ЧИННОСТІ ТОВАРНОГО ЗНА КА N 134893 (T-6186) ДАТА ПОДАННЯ 17.02.2010 0",
  "Doc_sum_base": 12049.37
};

const initApp = async function () {
  try {
    await Postgres.setBankPayment(row);
    console.log('READY');
    await mongoose.connectionPending;

    await Kafka.init();

    PaymentImport.init();


    log.info({'action': 'SERVICE STARTED'});
  } catch (error) {
    console.log(error)
  }
};


module.exports = initApp;
