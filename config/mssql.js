const sql = require('mssql');
const log = require('./logger');

const config = {
  /*user: process.env.MSSQL_USER,
  password: process.env.MSSQL_PASSWORD,
  server: process.env.MSSQL_SERVER,
  port: Number(process.env.MSSQL_PORT),
  database: process.env.MSSQL_DATABASE,*/
}

const executeProcedure = async function(name, inputParams = {}){    
  try {
    const pool = await new sql.ConnectionPool(config).connect();
    const request = pool.request();

    for (const key in inputParams) {
      if (inputParams.hasOwnProperty(key)) {
        request.input(key, inputParams[key]);
      }
    }
    
    const result = await request.execute(name);
    log.info({action: 'EXECUTE_PROCEDURE', name: name});
    return result;
  }
  catch (error) {
    log.error({action: 'EXECUTE_PROCEDURE', name: name, error: error.message});
  }
}

/**
 * @function
 * @async
 * @edscription
 * @param {Number} SubsystemCode - тип заявки  
 *  3 - Знаки
 *  4 - Промислові зразки
 *  5 - КЗПТ
 * @param {Date} PaymentDate - дата у форматі ISO
 * (new Date).toISOString()
 */


const GetUnprocessedPaymentsTMPP = async function(SubsystemCode, PaymentDate){

  const { recordset } = await executeProcedure('GetUnprocessedPaymentsTMPP', {
    SubsystemCode: SubsystemCode,
    PaymentDate: PaymentDate
  });
  return recordset;
}


module.exports = GetUnprocessedPaymentsTMPP;